﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

using CampUnitTest;

namespace CampUnitTest.Tests
{
    class CalculatorTests
    {
        [Test]
        public void Add_SevenAndTwo_ReturnsNine()
        {
            // Setup or Arrange
            var calculator = new Calculator();
            const int expected = 9;
            int parameterA = 7;
            int parameterB = 2;

            // Act
            var result = calculator.Add(parameterA, parameterB);

            // Assert
            Assert.AreEqual(expected, result);
        }
    }
}
