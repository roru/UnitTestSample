﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace campUnitTest.Tests
{
    class PersonTest
    {
        public void Compare_TwoDifferentInstances_ReturnZero()
        {
            // Setup
            var people1 = new People()
            {
                Name = "RORU1"
            };
            var people2 = new People()
            {
                Name = "RORU1"
            };
            const int expected = 0;

            // Act
            var result = people1.Compare(people2);

            // Assert
            Assert.AreEqual(expected, result);

        }

        public void Compare_TwoDifferentInstances_ReturnOne()
        {
            // Setup
            var people1 = new People()
            {
                Name = "RORU1"
            };
            var people2 = new People()
            {
                Name = "RORU2"
            };
            const int expected = 1;

            // Act
            var result = people1.Compare(people2);

            // Assert
            Assert.AreEqual(expected, result);

        }

        public void Compare_SameInstances_ReturnZero()
        {
            // Setup
            var people1 = new People()
            {
                Name = "RORU1"
            };
            var people2 = people1;
            const int expected = 0;

            // Act
            var result = people1.Compare(people2);

            // Assert
            Assert.AreEqual(expected, result);

        }
    }
}
